#!/usr/bin/env bash

flex -DYY_DECL="Token * yylex()" analyzer.l
gcc -o analyzer lexer.c analyzer.c