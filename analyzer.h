#define TRUE 1
#define FALSE 2

#define TOK_NUM 0
#define TOK_OP 1
#define TOK_PONT 2
#define TOK_RELOP 3
#define TOK_COND 4
#define TOK_COMMENT 5
#define TOK_ID 6
#define TOK_TIPO 7
#define TOK_LOOP 8
#define TOK_PROG 9
#define TOK_ERRO 10

#define PROG "programa"

#define ID "ID"

#define NUM "NUM"

#define INT "int"
#define REAL "real"
#define CHAR "char"

#define SOMA "+"
#define SUB "-"
#define MULT "*"
#define DIV "/"

#define BLOCO_ABRE "{"
#define BLOCO_FECHO "}"
#define PAR_ABRE "("
#define PAR_FECHO ")"
#define ATRIB ":="
#define PONTO_VIRGULA ";"

#define IF "se"
#define THEN "entao"
#define ELSE "senao"

#define WHILE "enquanto"
#define FOR "para"
#define DO "faca"

#define GT ">"
#define GE ">="
#define LT "<"
#define LE "<="
#define EQ "=="
#define NE "<>"

#define COMENT_ABRE "["
#define COMENT_FECHO "]"

typedef struct
{
    int tipo;
    char *valor;
    char column;
} Token;

extern Token *token();

extern void calculateColumnVal();

extern Token *yylex();
