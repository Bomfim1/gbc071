#include "lexer.h"
#include "analyzer.h"

int inicializa(char *nome)
{
    FILE *f = fopen(nome, "r");

    if (f == NULL)
    {
        return FALSE;
    }

    yyin = f;
    return TRUE;
}

Token *proximo_token()
{
    return yylex();
}

void imprime_token(Token *tok)
{
    printf("tipo:%d\t", tok->tipo);
    printf("valor:%s\t", tok->valor);
    printf("(line: %d, column: %d)\n", yylineno, tok->column);
}

int main(int argc, char const *argv[])
{
    char entrada[20];
    Token *tok;
    printf("\nDigite o nome do arq. de entrada: ");
    gets(entrada);

    inicializa(entrada);

    tok = proximo_token();
    while (tok != NULL)
    {
        // Aqui o token será retornado para o analisador sintático.
        // Para fins de teste o token é impresso na stdout.
        imprime_token(tok);
        tok = proximo_token();
    }

    return 0;
}
