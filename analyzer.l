%option noyywrap
%option nodefault
%option yylineno
%option outfile="lexer.c" header-file="lexer.h"

%{
  #include "analyzer.h"
  int first_column = 1;
  int last_column = 1;
  int current_line = 1;
%}

NUM [0-9]+

%%
[[:space:]]+ {} // Separadores
\[.* { 
  calculateColumnVal();
  return token(TOK_COMMENT, COMENT_ABRE, first_column);
  } 
.*\] { 
  calculateColumnVal();
  return token(TOK_COMMENT, COMENT_FECHO, first_column);}
"se" { 
  calculateColumnVal();
  return token(TOK_COND, IF, first_column);}
"entao" { 
  calculateColumnVal();
  return token(TOK_COND, THEN, first_column);}
"senao" { 
  calculateColumnVal();
  return token(TOK_COND, ELSE, first_column);}
"int" { 
  calculateColumnVal();
  return token(TOK_TIPO, INT, first_column);}
"real" { 
  calculateColumnVal();
  return token(TOK_TIPO, REAL, first_column);}
"char" { 
  calculateColumnVal();
  return token(TOK_TIPO, CHAR, first_column);}
"enquanto" {
  calculateColumnVal();
  return token(TOK_LOOP, WHILE, first_column);}
"faca" { 
  calculateColumnVal();
  return token(TOK_LOOP, DO, first_column);}
"programa" { 
  calculateColumnVal();
  return token(TOK_PROG, PROG, first_column);}
\>\= { 
  calculateColumnVal();
  return token(TOK_RELOP, GE, first_column);}
\> { 
  calculateColumnVal();
  return token(TOK_RELOP, GT, first_column);}
\< { 
  calculateColumnVal();
  return token(TOK_RELOP, LT, first_column);}
\<\= { 
  calculateColumnVal();
  return token(TOK_RELOP, LE, first_column);}
\=\= { 
  calculateColumnVal();
  return token(TOK_RELOP, EQ, first_column);
  }
\<\> { 
  calculateColumnVal();
  return token(TOK_RELOP, NE, first_column);
  }
[[:alpha:]][[:alnum:]]* { 
  calculateColumnVal();
  return token(TOK_ID, ID, first_column);
  } // Identificadores
{NUM}(\.{NUM})?(\E[+-]?{NUM})? { 
  calculateColumnVal();
  return token(TOK_NUM, NUM, first_column);
  }
\+ { 
  calculateColumnVal();
  return token(TOK_OP, SOMA, first_column);
  }
\- { 
  calculateColumnVal();
  return token(TOK_OP, SUB, first_column);
  }
\* { 
  calculateColumnVal();
  return token(TOK_OP, MULT, first_column);
  }
\/ { 
  calculateColumnVal();
  return token(TOK_OP, DIV, first_column);
  }
\( { 
  calculateColumnVal();
  return token(TOK_PONT, PAR_ABRE, first_column);
  }
\) { 
  calculateColumnVal();
  return token(TOK_PONT, PAR_FECHO, first_column);
  }
\{ { 
  calculateColumnVal();
  return token(TOK_PONT, BLOCO_ABRE, first_column);
  }
\} { 
  calculateColumnVal();
  return token(TOK_PONT, BLOCO_FECHO, first_column);
  }
\:\= { 
  calculateColumnVal();
  return token(TOK_PONT, ATRIB, first_column);
  }
\; { 
  calculateColumnVal();
  return token(TOK_PONT, PONTO_VIRGULA, first_column);
  }
\. { 
  calculateColumnVal();
  return token(TOK_ERRO, 0, first_column);
  }
[\n] {last_column = 0;}
%%

Token tok;
Token * token(int tipo, char* valor, int column) {
    tok.tipo = tipo;
    tok.valor = valor;
    tok.column = column;
    return &tok;
}
void calculateColumnVal(void){
  if(yylineno != current_line){
    last_column = 1;
    current_line = yylineno;
  }
  first_column = last_column;
  last_column += yyleng + 1;
}